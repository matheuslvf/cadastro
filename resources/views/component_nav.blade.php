
<nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded">

        <a style="margin-left: 10px" class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" >
            <ul class="navbar-nav">
                <li  class="nav-item" >
                    <a @if($current=="home") class="nav-link active" @else class="nav-link"  @endif href="/">Home</a>
                </li>
                <li  class="nav-item" >
                    <a @if($current=="produtos") class="nav-link active" @else class="nav-link"  @endif href="/produtos">Produtos</a>
                </li>

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"  @if($current=="categorias") class="nav-link dropdown-toggle active" @else class="nav-link dropdown-toggle"  @endif>Categorias</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a hr class="dropdown-item" href="/categorias/new">Cadastrar</a></li>
                            <li><a class="dropdown-item" href="/categorias">Lista de categorias</a></li>
                        </ul>
                </li>
            </ul>
        </div>    
</nav>

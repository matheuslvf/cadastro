@extends('layouts.app',['current'=>'home'])

@section('body')
    <div class="row">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
            <h5 class="card-title">Cadastro de produtos</h5>
            <h6 class="card-subtitle mb-2 text-muted">Register</h6>
            <p class="card-text">Apply now, dont forget save category previously.</p>
            <a href="#" class="btn btn-dark btn-sm">Cadastrar</a>
            <a href="#" class="btn btn-dark btn-sm">Another link</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
            <h5 class="card-title">Cadastro de categorias</h5>
            <h6 class="card-subtitle mb-2 text-muted">Register</h6>
            <p class="card-text">Apply now, dont forget save category previously.</p>
            <a href="#" class="btn btn-dark btn-sm">Cadastrar</a>
            <a href="#" class="btn btn-dark btn-sm">Another link</a>
            </div>
        </div>
    
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}"> 
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Cadastro de produtos</title>
    <style></style>
</head>
    <body>
        <div class="container">
            @component('component_nav', ['current' => $current])
            @endcomponent
            <main role="main">
                @hasSection ('body')
                    @yield('body')
                @endif
            </main>

        </div>
        
        <script src="{{asset('js/app.js')}}" type="text/javascript"></script>

        @hasSection ('javascript')
            @yield('javascript')
        @endif
        
    </body>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>



            {{-- <script type="text/javascript">
            $("#preco").mask("00.000.00");
            </script> --}}
</html>
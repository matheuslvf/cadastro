@extends('layouts.app',['current'=>'categorias'])
@section('body')
    <div class="card border">
        <div class="card-body">
            <h3>Cadastrar categoria:</h3><hr>
            <form action="/categorias" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name"><h6>Nome:</h6></label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Categoria">
                </div>
                <button style="margin-top: 8px" type="submit" class="btn btn-dark btn-sm">Cadastrar</button>
                <button style="margin-top: 8px" type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection
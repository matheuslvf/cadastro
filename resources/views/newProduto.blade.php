@extends('layouts.app',['current'=>'produtos'])
@section('body')
    <div class="card border">
        <div class="card-body">
            <h3>Cadastrar produto:</h3><hr>
            <form action="/produtos" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name"><h6>Nome:</h6></label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Ex: "><br>

                    <label for="phone"><h6>Estoque:</h6></label>
                    <input type="number" class="form-control" name="estoque" id="name" placeholder="Quantidade em estoque" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"><br>

                    <label for=""><h6>Preço:</h6></label>
                    <input type="number" step="0.01" class="form-control" name="preco" min="0.01" id="preco" placeholder="R$"><br>
                
                    <label for=""><h6>Categoria:</h6></label>
                    <select class="form-select" aria-label="Default select example" name="categoria_id">
                        <option selected>Escolha a categoria</option>
                        @foreach ($cats as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select> <br>

                </div>
                <button style="margin-top: 8px" type="submit" class="btn btn-dark btn-sm">Cadastrar</button>
                <button style="margin-top: 8px" type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
            </form>
        </div>
    </div>
@endsection
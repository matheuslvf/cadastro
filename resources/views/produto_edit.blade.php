@extends('layouts.app',['current'=>'categorias'])
@section('body')
<div class="card border">
    <div class="card-body">
        <h3>Editar produto:</h3><hr>
        <form action="/produtos/{{$prod->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name"><h6>Nome:</h6></label>
                <input type="text" class="form-control" name="name" id="name" value="{{$prod->name}}"><br>

                <label for="phone"><h6>Estoque:</h6></label>
                    <input type="number" class="form-control" name="estoque"  pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" value="{{$prod->estoque}}"><br>

                    <label for=""><h6>Preço:</h6></label>
                    <input type="number" step="0.01" class="form-control" name="preco" min="0.01" id="preco" value="{{$prod->preco}}"><br>
                
                    <label><h6>Categoria:</h6></label>
                    <select  required class="form-select" aria-label="Default select example" name="categoria_id">
                        <option></option>
                        @foreach ($cats as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select> <br>

            </div>
            <button style="margin-top: 8px" type="submit" class="btn btn-dark btn-sm">Atualizar</button>
            <button style="margin-top: 8px" type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
        </form>
    </div>
</div>
@endsection
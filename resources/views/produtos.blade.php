@extends('layouts.app', ['current'=>'produtos'])

@section('body')
    <div class="card border">
        <div class="card-body">
            <h4 class="card-title">Lista de produtos:</h4><hr>
            @if (count($prods) > 0 )
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Estoque</th>
                        <th>Preço</th>
                        <th>Categoria</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($prods as $prod)
                        <tr>
                            <td>#{{$prod->id}}</td>
                            <td>{{$prod->name}}</td>
                            <td>{{$prod->estoque}}</td>
                            <td id="preco">R${{$prod->preco}}</td>
                            <td>{{$prod->categoria->name}}</td>
                            <td>
                                <a href="/produtos/edit/{{$prod->id}}" class="btn btn-dark btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                    width="24" height="24"
                                    viewBox="0 0 172 172"
                                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M131.86947,14.33333c-2.15,0 -3.59173,0.72227 -5.02507,2.1556l-14.27735,14.27734l-10.13411,10.13411l-80.93294,80.93294v28.66667h28.66667l105.3444,-105.3444c2.86667,-2.86667 2.86667,-7.16947 0,-10.03613l-18.63054,-18.63053c-1.43333,-1.43333 -2.86106,-2.1556 -5.01106,-2.1556zM131.86947,31.53613l8.5944,8.5944l-9.26627,9.26628l-8.5944,-8.5944zM112.46907,50.93652l8.5944,8.5944l-76.63574,76.63574h-8.5944v-8.5944z"></path></g></g></svg>
                                </a>
                                <a href="/produtos/delete/{{$prod->id}}" class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="right" title="Excluir">
                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                        width="24" height="24"
                                        viewBox="0 0 172 172"
                                        style=" fill:#000000;"><g transform=""><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M77.44759,14.33333c-3.70517,0 -7.24517,1.46424 -9.86817,4.08724l-3.07943,3.07943h-35.83333c-2.58456,-0.03655 -4.98858,1.32136 -6.29153,3.55376c-1.30295,2.2324 -1.30295,4.99342 0,7.22582c1.30295,2.2324 3.70697,3.59031 6.29153,3.55376h114.66667c2.58456,0.03655 4.98858,-1.32136 6.29153,-3.55376c1.30295,-2.2324 1.30295,-4.99342 0,-7.22582c-1.30295,-2.2324 -3.70697,-3.59031 -6.29153,-3.55376h-35.83333l-3.07943,-3.07943c-2.61583,-2.623 -6.163,-4.08724 -9.86816,-4.08724zM31.28418,50.16667l10.94596,95.05632c0.946,7.095 7.05502,12.44368 14.20736,12.44368h59.111c7.15233,0 13.26819,-5.34018 14.22135,-12.49968l10.94597,-95.00032z"></path></g></g></g></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach                       
                </tbody>
            </table>                
            @else
                <h5 class="title">Não existem produtos cadastrados!</h5><br>
            @endif
            <div class="card-footer">
                <button class="btn btn-dark btn-sm" role="button" onclick="novoProduto()">Novo produto</a>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="dlgProdutos">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" class="form-horizontal" id="formProduto">
                    <div class="modal-header">
                        <h5>Novo produto</h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="id" class="form-control">
                        <div class="form-group">
                            <label for="name">Nome do produto:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="preco">Preço:</label>
                            <div class="input-group">
                                <input type="number" class="form-control" id="preco">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="estoque">Quantidade:</label>
                            <div class="input-group">
                                <input type="number" class="form-control"  id="estoque">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nomeProduto">Nome do produto:</label>
                            <div class="input-gruoup">
                                <select class="form-control" id="categoria"></select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button class="btn btn-secondary" onclick="hide()">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function novoProduto(){
            $('#dlgProdutos').modal('show')
        }

        function hide(){
            $('#dlgProdutos').modal('hide')
        }
    </script>
    
@endsection
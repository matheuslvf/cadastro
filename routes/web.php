<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ProdutoController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

//Categorias
Route::get('/categorias', [CategoriaController::class, 'index']);
Route::get('/categorias/new', [CategoriaController::class, 'create']);
Route::post('/categorias', [CategoriaController::class, 'store']);
Route::post('/categorias/{id}', [CategoriaController::class, 'update']);
Route::get('/categorias/delete/{id}', [CategoriaController::class, 'destroy']);
Route::get('/categorias/edit/{id}', [CategoriaController::class, 'edit']);

//produtos
Route::get('/produtos', [ProdutoController::class, 'index']);
Route::get('/produtos/new', [ProdutoController::class, 'create']);
Route::post('/produtos', [ProdutoController::class, 'store']);
Route::post('/produtos/{id}', [ProdutoController::class, 'update']);
Route::get('/produtos/delete/{id}', [ProdutoController::class, 'destroy']);
Route::get('/produtos/edit/{id}', [ProdutoController::class, 'edit']);
